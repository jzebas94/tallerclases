/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.algorithmintegration.students;

import com.algorithmintegration.service.Actividades;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Alvaro Gutierrez
 */
public class AlvaroGutierrez implements Actividades{

    /**
     *Ordena una lista de mayor a menos e imprime numeros pares.
     * @param lista
     */

    @Override
    public void ordenar(List <Integer> lista){
        //Se recibe una lista en dsorden y ordenarla de mayor a menos valor 
        //solo números pares
        System.out.println("Lista desordenada: " + lista);
        lista.sort(Comparator.reverseOrder());
        System.out.println("Lista ordenada: " + lista);
        lista.removeIf(n -> (n%2 != 0));
        System.out.println("Sólo los números pares de la Lista: " + lista);
    }

    /**
     *Imprime el nombre con la primera letra en mayusculas.
     * @param nombre
     */
    @Override
    public void unirNombre(String nombre){
        //imprimir el nombre que llega solo la letra inicial en mayuscula 
        //y las demas en minuscula sin espacios
        System.out.println("Nombre inicial: " + nombre);
        String[] fullname = nombre.split(" ");
        for (int i=0;i<fullname.length;i++)
            fullname [i] = fullname[i].toUpperCase().charAt(0) + fullname[i].substring(1).toLowerCase();
        System.out.print("Nombre final: ");
        for (String name : fullname)
            System.out.print(name + " ");
        System.out.println("");
    }

    /**
     *Imprime el nombre al reves.
     */
    @Override
    public void imprimirNombre(){
        //Declarar el nombre propio en una variable e imprimirlo al reves
        String name = "Alvaro Raul Gutierrez Tovar";
        StringBuilder strb = new StringBuilder(name);
        name = strb.reverse().toString();
        System.out.println(name);
    }

    /**
     *Cifra una contraseña.
     * @param password
     */
    @Override
    public void cifrarPalabra(String password){
        //utilizar un algoritmo de cifrado para encriptar la contraseña
        System.out.println(Base64.getEncoder().encodeToString(password.getBytes(StandardCharsets.UTF_8)));
    }

    /**
     *Recibe un string y un int, convierte el string a entero y suma los dos numeros.
     * @param num1
     * @param num2
     * @exception AssertionError
     */
    @Override
    public void convertirNumero(String num1, int num2){
        // convertir el primer digito a entero para sumarlo, controlar una posible 
        //excepcion pues al momento de convertir se puede generar e imprimir el mensale
        //de error aca mismo en el metodo
        try {
            int fNumber = Integer.parseInt(num1);
            int suma = fNumber + num2;
            System.out.println("Suma: " + suma);
        }
        catch (Exception | AssertionError e){
            System.out.println("Error: "+e);
        }
    }
}
