/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.algorithmintegration.students;

import com.algorithmintegration.service.Actividades;
import java.util.List;

/**
 *
 * @author s2694883
 */
public class DavidOchoa implements Actividades {
    
    @Override
    public void ordenar(List <Integer> lista){
        //Se recibe una lista en dsorden y ordenarla de mayor a menos valor 
        //solo números PRIMOS
    }
    
    @Override
    public void unirNombre(String nombre){
        //imprimir el nombre que llega solo la letra inicial en mayuscula 
        //y las demas en minuscula sin espacios
    }
    
    @Override
    public void imprimirNombre(){
        //Declarar el nombre propio en una variable e imprimirlo al reves
    }
    
    @Override
    public void cifrarPalabra(String password){
        //utilizar un algoritmo de cifrado para encriptar la contraseña
    }
    
    @Override
    public void convertirNumero(String num1, int num2){
        // convertir el primer digito a entero para sumarlo, controlar una posible 
        //excepcion pues al momento de convertir se puede generar e imprimir el mensale
        //de error aca mismo en el metodo
    }
}
