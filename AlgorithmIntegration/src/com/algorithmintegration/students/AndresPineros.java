/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.algorithmintegration.students;

import com.algorithmintegration.service.Actividades;
import java.util.*;

/**
 *
 * @author s2694883
 */
public class AndresPineros implements Actividades{
    
    @Override
    public void ordenar(List <Integer> lista){
        //Se recibe una lista en dsorden y ordenarla de mayor a menos valor 
        //solo números multiplos de 5
       
        
        lista = new ArrayList<>();
        int numero;
        String flag;

        System.out.println("Ingrese números a la lista. Oprima 'Y' para terminar");
        Scanner reader = new Scanner(System.in);
        
        do{
            
                System.out.print("Ingrese un número a la lista: ");
                numero = reader.nextInt();
                System.out.print("¿Desea terminar su lista Y/N?: ");
                flag = reader.next();

                if(numero%5 ==0){
                    lista.add(numero);
                }
                else{
                    System.out.println("Ingrese unicamente números multiplos de 5 ");
                }
                            
        }
        while (!flag.equalsIgnoreCase("Y"));
       

        Collections.sort(lista);      
        System.out.println("La lista organizada es: "+ lista);  
        reader.close(); 
    }
    
    
    @Override
    public void unirNombre(String nombre){
        //imprimir el nombre que llega solo la letra inicial en mayuscula 
        //y las demas en minuscula sin espacios
                  
                
            nombre = "nombre";
            String firstLtr = nombre.substring(0, 1);
            String restLtrs = nombre.substring(1, nombre.length());
              
            firstLtr = firstLtr.toUpperCase();
            nombre = firstLtr + restLtrs;
            System.out.println("The modified string is: "+nombre);
            
        
        }        
    
    @Override
    public void imprimirNombre(){
        //Declarar el nombre propio en una variable e imprimirlo al reves
        String nombre;          
        String nombreInvertido = "";
        Scanner reader = new Scanner(System.in);
        
        System.out.println("Introduzca su nombre: ");
        nombre = reader.next();

        for (int indice = nombre.length() - 1; indice >= 0; indice--) {
            nombreInvertido += nombre.charAt(indice);
        }
        System.out.println("Nombre invertido: " + nombreInvertido);

        reader.close();
    }
    
    @Override
    public void cifrarPalabra(String password){
        //utilizar un algoritmo de cifrado para encriptar la contraseña
        Scanner reader = new Scanner(System.in);

        password = reader.next();
        char array[] = password.toCharArray();

        for(int i=0;i<array.length;i++){
            array[i]=(char)(array[i]+ (char)5);
        }

        String PassCifrado = String.valueOf(array);
        System.out.println("El texto cifrado es: "+ PassCifrado);
        
        reader.close();
    }
    
    @Override
    public void convertirNumero(String num1, int num2){
        // convertir el primer digito a entero para sumarlo, controlar una posible 
        //excepcion pues al momento de convertir se puede generar e imprimir el mensale
        //de error aca mismo en el metodo
        
    }
}
