/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.algorithmintegration.students;

import com.algorithmintegration.service.Actividades;
import java.util.*;
import java.io.*;
import java.lang.*;

/**
 *
 * @author Freddy Montañez
 * @version 1.0
 * @
 */
public class FreddyMontanez implements Actividades{
    
    @Override
    public void ordenar(List <Integer> lista){
        //Se recibe una lista en dsorden y ordenarla de mayor a menos valor 
        //solo números PRIMOS      
        Collections.sort(lista, Collections.reverseOrder());
        System.out.println(lista);
       
    }
    
    @Override
    public void unirNombre(String nombre){
        //imprimir el nombre que llega solo la letra inicial en mayuscula 
        //y las demas en minuscula sin espacios
        String nombreMin=nombre.toLowerCase();
        char[] arrayName = nombreMin.toCharArray(); 
        arrayName[0]=Character.toUpperCase(arrayName[0]);
        String nameUpper = new String(arrayName);      
        System.out.println(nameUpper.replaceAll("\\s",""));        
    }
    
    @Override
    public void imprimirNombre(){
        //Declarar el nombre propio en una variable e imprimirlo al reves
        String myName= "Freddy Montañez";
        StringBuilder myNameInv= new StringBuilder(myName);
        myName=myNameInv.reverse().toString();
        System.out.println(myName);
    }
    
    @Override
    public void cifrarPalabra(String password){
        //utilizar un algoritmo de cifrado para encriptar la contraseña
        String encPass = Base64.getEncoder().encodeToString(password.getBytes());
        System.out.println(encPass);
    }
    
    @Override
    public void convertirNumero(String num1, int num2){
        // convertir el primer digito a entero para sumarlo, controlar una posible 
        //excepcion pues al momento de convertir se puede generar e imprimir el mensale
        //de error aca mismo en el metodo
        try {
       Integer nnum1=Integer.parseInt(num1);
       System.out.println(nnum1+num2);
        }catch(NumberFormatException nfe){
            System.out.println("Ups!! Algo salió mal... Por favor verifica que tu primer dígito sea un Numero entero>  "+nfe);
        }
                
    }
}
