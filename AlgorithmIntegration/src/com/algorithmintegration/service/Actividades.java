/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.algorithmintegration.service;

import java.util.List;

/**
 *
 * @author s2694883
 */
public interface Actividades {
    
    public void ordenar(List <Integer> lista);
    
    public void unirNombre(String nombre);
    
    public void imprimirNombre();
    
    public void cifrarPalabra(String password);
    
    public void convertirNumero(String num1, int num2);
}
